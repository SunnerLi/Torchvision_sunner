from torch.autograd import Variable
import torchvision_sunner.transforms as sunnertransforms
import torchvision_sunner.data as sunnerData
import torchvision.transforms as transforms
import skimage.io as io
import numpy as np
import torch
import cv2

"""
    This example illustrates transforming the tensor image into numpy.ndarray directly.
"""

if __name__ == '__main__':
    # Define the dataset and loader
    dataset = sunnerData.ImageDataset(
        root_list = ['./waiting_for_you_dataset/real_world', './waiting_for_you_dataset/wait'],
        use_cv = False,
        sample_method = sunnerData.OVER_SAMPLING,
        transform = transforms.Compose([
            sunnertransforms.Rescale((160, 320)),
            sunnertransforms.ToTensor(),
            sunnertransforms.ToFloat(),
            sunnertransforms.Transpose(sunnertransforms.BHWC2BCHW),
            sunnertransforms.Normalize()
        ]) 
    ) 
    loader = sunnerData.ImageLoader(dataset, batch_size=4, shuffle=True, num_workers = 2)

    # Work
    for real_img, wait_img in loader:
        batch_img = Variable(wait_img)

        # Transfer variable object to numpy object
        batch_img = sunnertransforms.asImg(batch_img, size = (320, 640))

        # Show
        # cv2.imshow('show_window', batch_img[0])
        # cv2.waitKey()
        io.imsave('picture.png', batch_img[0])
        break