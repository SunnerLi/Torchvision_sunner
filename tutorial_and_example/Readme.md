# Torchvision_sunner
### The flexible extension of torchvision toward multiple image space

[![Packagist](https://img.shields.io/badge/Version-18.6.28-yellow.svg)]()
[![Packagist](https://img.shields.io/badge/Pytorch-0.3.0-red.svg)]()
[![Packagist](https://img.shields.io/badge/Torchvision-0.2.0-red.svg)]()
[![Packagist](https://img.shields.io/badge/Python-3.5.2-blue.svg)]()
[![Packagist](https://img.shields.io/badge/OpenCV-3.1.0-brightgreen.svg)]()
[![Packagist](https://img.shields.io/badge/skImage-0.13.1-green.svg)]()

Before you run the example, you should copy the latest wrapper folder from the parent folder:
```
$ cp ../torchvision_sunner .
```