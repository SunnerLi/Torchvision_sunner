# Torchvision_sunner
### The flexible extension of torchvision toward multiple image space

[![Packagist](https://img.shields.io/badge/Version-18.4.4-yellow.svg)]()
[![Packagist](https://img.shields.io/badge/Pytorch-0.3.0-red.svg)]()
[![Packagist](https://img.shields.io/badge/Torchvision-0.2.0-red.svg)]()
[![Packagist](https://img.shields.io/badge/Python-3.5.2-blue.svg)]()
[![Packagist](https://img.shields.io/badge/OpenCV-3.1.0-brightgreen.svg)]()
[![Packagist](https://img.shields.io/badge/skImage-0.13.1-green.svg)]()

Before you run the testing code, you should download the dataset here:
```
# Move to the test folder
$ cd ~/Torchvision_sunner/test

# The result should be like this
$ ls
Readme.md  test.py  waiting_for_you_dataset

# Run!
$ python3 test.py

# ...(Some testing log)...
# ----------------------------------
#         Pass the testing !
# ----------------------------------
```